import gc
import json
import sys
import threading
import time
from functools import wraps
import pymysql
import requests
import serial
import serial.tools.list_ports
from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, jsonify, make_response, session, request
from pygame import mixer
from serial import Serial, SerialException

from data_frame_handler import catch_frame, makeFrameByte, START_FRAME_BYTE, END_FRAME_BYTE

IS_DEBUG_PRINT_VISSABLE = True
DBG_TAG = ""

isSerialReceiverLoopRun = True
isSerialConnClosed = False

CONF_APP_FILE_PATH = "config/config_app.json"
CONF_DB_FILE_PATH = "config/config_db.json"
CONF_WEBSERVICE_FILE_PATH = "config/config_webservice.json"
CONF_WEBSERVER_FILE_PATH = "config/config_webserver.json"
CONF_SERIAL_FILE_PATH = "config/config_serial.json"

WEBSERVICE_HOST_URL = ""
WEBSERVICE_APP_NAME = ""
WEBSERVICE_IS_LOG_ACTIVE = False
WEBSERVICE_REPORTER_NOFIFICATION_EMAILS = ""

DB_HOST = ""
DB_PORT = ""
DB_USER = ""
DB_PASSWORD = ""
DB_NAME = ""
DB_CHARSET = ""

SERIAL_PORT = ""
SERIAL_BAUDRATE = ""
SERIAL_PARITY = ""
SERIAL_STOPBITS = ""
SERIAL_BYTESIZE = ""

EXPECTED_MAX_FRAME_SIZE = 20

WEBSERVER_ANDROID_HOST = ""
WEBSERVER_ANDROID_PORT = ""
WEBSERVER_ANDROID_DATA_FRAME_URL = ""
WEBSERVER_ANDROID_DATA_FRAME_ERROR_URL = ""
DELAY_MS = 30  # in miliseconds

pymysql.install_as_MySQLdb()
app = Flask(__name__)


def initSerial(port, baudrate, parity, stopbits, bytesize):
    m_serial = None

    try:
        m_serial = Serial(
            port=port,
            baudrate=baudrate,
            parity=parity,
            stopbits=stopbits,
            bytesize=bytesize
        )
    except SerialException as e:
        print(str(e), file=sys.stderr)

    return m_serial


def init_app_credentials():
    global IS_DEBUG_PRINT_VISSABLE
    global DBG_TAG
    try:
        with open(CONF_APP_FILE_PATH) as json_file:
            data = json.load(json_file)
            IS_DEBUG_PRINT_VISSABLE = bool(data['config']['is_debug_print_vissable'])
            print(IS_DEBUG_PRINT_VISSABLE)
            DBG_TAG = data['config']['debug_tag']


    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
    except ValueError:
        print("Could not read config file")
    except:
        print("Unexpected error:", sys.exc_info()[0])


def init_db_credentials():
    global DB_HOST
    global DB_PORT
    global DB_USER
    global DB_PASSWORD
    global DB_NAME
    global DB_CHARSET
    try:
        with open(CONF_DB_FILE_PATH) as json_file:
            data = json.load(json_file)
            DB_HOST = data['config']['host']
            DB_PORT = data['config']['port']
            DB_USER = data['config']['user']
            DB_PASSWORD = data['config']['password']
            DB_NAME = data['config']['db']
            DB_CHARSET = data['config']['charset']


    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
    except ValueError:
        print("Could not read config file")
    except:
        print("Unexpected error:", sys.exc_info()[0])


def init_webservice_credentials():
    global WEBSERVICE_HOST_URL
    global WEBSERVICE_APP_NAME
    global WEBSERVICE_IS_LOG_ACTIVE
    global WEBSERVICE_REPORTER_NOFIFICATION_EMAILS

    try:
        with open(CONF_WEBSERVICE_FILE_PATH) as json_file:
            data = json.load(json_file)
            WEBSERVICE_HOST_URL = data['config']['host_url']
            WEBSERVICE_APP_NAME = data['config']['app_name']
            WEBSERVICE_IS_LOG_ACTIVE = data['config']['is_log_active']
            WEBSERVICE_REPORTER_NOFIFICATION_EMAILS = data['config']['reporter_notification_emails']

    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
    except ValueError:
        print("Could not read webservice config file")
    except:
        print("Unexpected error:", sys.exc_info()[0])

def init_webserver_credentials():

    global WEBSERVER_ANDROID_HOST
    global WEBSERVER_ANDROID_PORT
    global WEBSERVER_ANDROID_DATA_FRAME_URL
    global WEBSERVER_ANDROID_DATA_FRAME_ERROR_URL



    try:
        with open(CONF_WEBSERVER_FILE_PATH) as json_file:
            data = json.load(json_file)
            WEBSERVER_ANDROID_HOST = data['config']['host_url']
            WEBSERVER_ANDROID_PORT = data['config']['port']
            print_debug(IS_DEBUG_PRINT_VISSABLE,DBG_TAG,WEBSERVER_ANDROID_HOST)
            print_debug(IS_DEBUG_PRINT_VISSABLE,DBG_TAG,WEBSERVER_ANDROID_PORT)

            WEBSERVER_ANDROID_DATA_FRAME_URL = WEBSERVER_ANDROID_HOST + ":" + WEBSERVER_ANDROID_PORT + "/data_frame"
            WEBSERVER_ANDROID_DATA_FRAME_ERROR_URL = WEBSERVER_ANDROID_HOST + ":" + WEBSERVER_ANDROID_PORT + "/data_frame_error"



    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
    except ValueError:
        print("Could not read webservice config file")
    except:
        print("Unexpected error:", sys.exc_info()[0])


def init_serial_credentials():
    global SERIAL_PORT
    global SERIAL_BAUDRATE
    global SERIAL_PARITY
    global SERIAL_STOPBITS
    global SERIAL_BYTESIZE

    try:
        with open(CONF_SERIAL_FILE_PATH) as json_file:
            data = json.load(json_file)
            SERIAL_PORT = data['config']['port']
            SERIAL_BAUDRATE = data['config']['baudrate']
            SERIAL_PARITY = data['config']['parity']
            SERIAL_STOPBITS = data['config']['stopbits']
            SERIAL_BYTESIZE = data['config']['bytesize']

    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
    except ValueError:
        print("Could not read config file")
    except:
        print("Unexpected error:", sys.exc_info()[0])


def save_serial_credentials(port, baudrate, parity, stopbits, bytesize):
    global SERIAL_PORT
    global SERIAL_BAUDRATE
    global SERIAL_PARITY
    global SERIAL_STOPBITS
    global SERIAL_BYTESIZE

    data = {'config': {'port': port,
                       'baudrate': baudrate,
                       'parity': parity,
                       'stopbits': stopbits,
                       'bytesize': bytesize}}
    SERIAL_PORT = port
    SERIAL_BAUDRATE = baudrate
    SERIAL_PARITY = parity
    SERIAL_STOPBITS = stopbits
    SERIAL_BYTESIZE = bytesize

    with open(CONF_SERIAL_FILE_PATH, 'w') as outfile:
        json.dump(data, outfile, indent=4)


@app.before_request
def make_session_permanent():
    session.permanent = True


# dev
def db_connection():
    conn = pymysql.connect(host=DB_HOST,
                           port=DB_PORT,
                           user=DB_USER,
                           password=DB_PASSWORD,
                           db=DB_NAME,
                           charset=DB_CHARSET,
                           cursorclass=pymysql.cursors.DictCursor)

    cur = conn.cursor()
    return cur, conn


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            result = jsonify(dict(result={"status": "not_ok",
                                          "code": "-1",
                                          "description": "You are not log in!"}))
            return result

    return wrap


@app.route("/is_user_logged_in", methods=["GET"])
@login_required
def is_user_logged_in():
    visitLog(WEBSERVICE_IS_LOG_ACTIVE, "is_user_logged_in")
    result = jsonify(dict(result={"status": "ok",
                                  "code": "1",
                                  "description": "You are logged in!"}))
    return result


@app.route("/log_out", methods=["POST"])
@login_required
def logout():
    visitLog(WEBSERVICE_IS_LOG_ACTIVE, "log_out")
    session.clear()
    result = jsonify(dict(result={"status": "ok",
                                  "code": "1",
                                  "description": "You are logged out!"}))
    gc.collect()
    return result


@app.route("/")
@login_required
def main():
    return "Welcome!"


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


def visitLog(is_active, endpointName):
    if is_active:
        insertQuery = "INSERT INTO ventilaid.visit_log (endpoint_name, visit_datetime) VALUES( %s , NOW());"

        cur = None
        conn = None

        try:
            cur, conn = db_connection()
            cur.execute(insertQuery, endpointName)
            conn.commit()
            gc.collect()

        except Exception as e:
            print(str(e))

        finally:
            if cur is not None:
                cur.close()
            if conn is not None:
                conn.close()


@app.route("/enter_setting", methods=["POST"])
def enter_setting():
    visitLog(WEBSERVICE_IS_LOG_ACTIVE, "enter_setting")
    result = None
    cur = None

    breaths_sett = 0
    try:
        if request.method == "POST":
            setting_name = request.form.get('setting_name', default="", type=str)
            setting_value = request.form.get('setting_value', default="", type=str)
            if setting_name == "breaths_sett":
                breaths_sett = setting_value

            print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG,
                        "setting_name: " + setting_name + " |  " + "setting_value: " + setting_value)
            frame = makeFrameByte(0, 0, breaths_sett, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

            serialTransmitter(frame)

            result = jsonify(dict(result={"status": "ok",
                                          "code": "1",
                                          "description": "Correct request"}))

    except Exception as e:
        result = jsonify(dict(result={"status": "not_ok",
                                      "code": "-1",
                                      "description": "Incorrect irequest" + str(e)}))
        print(str(e))
    return result


@app.route("/send_frame", methods=["POST"])
def send_frame():
    visitLog(WEBSERVICE_IS_LOG_ACTIVE, "send_frame")
    result = None
    cur = None

    breaths_sett = 0
    try:
        if request.method == "POST":
            hi_press = request.form.get('hi_press', default="", type=str)
            lo_press = request.form.get('lo_press', default="", type=str)
            breaths = request.form.get('breaths', default="", type=str)
            breath_pro = request.form.get('breath_pro', default="", type=str)
            air_vol = request.form.get('air_vol', default="", type=str)
            ref_rate = request.form.get('ref_rate', default="", type=str)
            f0 = request.form.get('f0', default="", type=str)
            f1 = request.form.get('f1', default="", type=str)
            f2 = request.form.get('f2', default="", type=str)
            f3 = request.form.get('f3', default="", type=str)
            f4 = request.form.get('f4', default="", type=str)
            f5 = request.form.get('f5', default="", type=str)
            f6 = request.form.get('f6', default="", type=str)

            num_hi_press = int(hi_press)
            num_lo_press = int(lo_press)
            num_breaths = int(breaths)
            num_breath_pro = int(breath_pro)
            num_air_vol = int(air_vol)
            num_ref_rate = int(ref_rate)
            num_f0 = int(f0)
            num_f1 = int(f1)
            num_f2 = int(f2)
            num_f3 = int(f3)
            num_f4 = int(f4)
            num_f5 = int(f5)
            num_f6 = int(f6)

            frame = makeFrameByte(num_hi_press, num_lo_press, num_breaths, num_breath_pro, num_air_vol,
                                  num_ref_rate, num_f0, num_f1, num_f2, num_f3, num_f4, num_f5, num_f6)

            serialTransmitter(frame)

            result = jsonify(dict(result={"status": "ok",
                                          "code": "1",
                                          "description": "Correct request"}))

    except Exception as e:
        result = jsonify(dict(result={"status": "not_ok",
                                      "code": "-1",
                                      "description": "Incorrect irequest" + str(e)}))
        print(str(e))
    return result


@app.route("/set_serial_port_settings", methods=["POST"])
def set_serial_port_settings():
    visitLog(WEBSERVICE_IS_LOG_ACTIVE, "set_serial_port_settings")
    result = None
    cur = None
    global serial_conn
    global isSerialReceiverLoopRun
    global isSerialConnClosed

    breaths_sett = 0
    try:
        if request.method == "POST":
            port = request.form.get('port', default="", type=str)
            baudrate = request.form.get('baudrate', default="", type=str)
            parity = request.form.get('parity', default="", type=str)
            stopbits = request.form.get('stopbits', default="", type=str)
            bytesize = request.form.get('bytesize', default="", type=str)

            baudrate_num = int(baudrate)
            stopbits_num = int(stopbits)
            bytesize_num = int(bytesize)

            save_serial_credentials(port, baudrate_num, parity, stopbits_num, bytesize_num)

            isSerialReceiverLoopRun = False

            # It is used to prevent for creation new serial connection, when old connection is still working.
            while (isSerialConnClosed == False):
                print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "I am still waiting for new serialconnection")
                time.sleep(.5)
            print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "New serial connection done")

            serial_conn = serial_conn = initSerial(port, baudrate_num, parity, stopbits_num, bytesize_num)
            isSerialReceiverLoopRun = True
            isSerialConnClosed = False
            print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG,
                        "Status of serial connection after previous colose: " + str(serial_conn.is_open))

            result = jsonify(dict(result={"status": "ok",
                                          "code": "1",
                                          "description": "Correct request"}))
    except Exception as e:
        result = jsonify(dict(result={"status": "not_ok",
                                      "code": "-1",
                                      "description": "Incorrect irequest" + str(e)}))
        print(str(e))
    return result


@app.route("/get_serial_ports_list", methods=["GET"])
def get_serial_ports_list():
    visitLog(WEBSERVICE_IS_LOG_ACTIVE, "get_serial_ports_list")
    result = None
    cur = None

    breaths_sett = 0
    try:
        available_ports = get_available_serial_ports()

        result = jsonify(dict(result={"status": "ok",
                                      "code": "1",
                                      "description": "Correct request"}, available_ports=available_ports))

    except Exception as e:
        result = jsonify(dict(result={"status": "not_ok",
                                      "code": "-1",
                                      "description": "Incorrect irequest" + str(e)}))
        print(str(e))
    return result


@app.route("/get_serial_current_settings", methods=["GET"])
def get_serial_current_settings():
    visitLog(WEBSERVICE_IS_LOG_ACTIVE, "get_serial_current_settings")
    result = None
    cur = None

    breaths_sett = 0
    try:
        current_settings = get_current_serial_settings()
        # i have generic approach in android app so I do  list() here :)
        current_settings_list = list()
        current_settings_list.append(current_settings)

        result = jsonify(dict(result={"status": "ok",
                                      "code": "1",
                                      "description": "Correct request"}, current_settings=current_settings_list))

    except Exception as e:
        result = jsonify(dict(result={"status": "not_ok",
                                      "code": "-1",
                                      "description": "Incorrect request" + str(e)}))
        print(str(e))
    return result


def runScheduledTasks():
    # init BackgroundScheduler job
    scheduler = BackgroundScheduler({'apscheduler.timezone': 'UTC'})
    # in your case you could change seconds to hours
    # scheduler.add_job(some_job1, trigger='interval', seconds=60, max_instances=1)
    # scheduler.add_job(some_job2, trigger='interval', minutes=30, max_instances=1)
    scheduler.start()


class myThread(threading.Thread):
    def __init__(self, threadLock, threadID, serial_mode):
        threading.Thread.__init__(self)

        self.threadID = threadID
        self.serial_mode = serial_mode

    def run(self):
        # Get lock to synchronize threads

        thread_handler(self.serial_mode)
        # Free lock to release next thread


def thread_handler(mode):
    if (mode == "rx"):
        serialReceiver()

    elif (mode == "tx"):
        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "Tx Thread")


    elif (mode == 'web'):
        runScheduledTasks()
        init_webservice_credentials()
        init_db_credentials()

        if __name__ == "__main__":
            app.secret_key = "asasasasas"  # os.urandom(16)
            app.run(host='0.0.0.0', debug=False)


def serialReceiver():
    global isSerialReceiverLoopRun
    global isSerialConnClosed
    global serial_conn

    while True:
        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "Main receiver infitite loop")
        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, str(serial_conn))
        isSerialConnClosed = True # it is needed to break infinite loop from method set_serial_port_settings(), in scenario when we are passing new BUT INVALID serial connection parameters
        while isSerialReceiverLoopRun:
            try:
                print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "isRun loop")
                if serial_conn is not None:
                    print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "serial not null")
                    print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "isRun: " + str(isSerialReceiverLoopRun))
                    try:
                        out = serial_conn.read_until(END_FRAME_BYTE, size=EXPECTED_MAX_FRAME_SIZE + 1)
                        if len(out) > EXPECTED_MAX_FRAME_SIZE:
                            print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "za dużo!")
                            raise ValueError('App catch more data into frame than you specified. '
                                             'It means that app did not found >end_frame_byte<  but catches more bytes that'
                                             'specified as max >EXPECTED_MAX_FRAME_SIZE<. '
                                             'Check serial settings in app and your device')
                        frame = catch_frame(out, START_FRAME_BYTE, END_FRAME_BYTE)
                        # if frame is not None: #todo
                        # insert_frame_into_db(frame)
                    except ValueError as e:
                        frame = None
                        print(str(e), file=sys.stderr)
                    if (frame != None):
                        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "Correct frame!")
                        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, str(frame))
                        if (len(frame) == 5):
                            send_status_data_to_android(WEBSERVER_ANDROID_DATA_FRAME_URL, frame[0] / 1000 + 60,
                                                        frame[1] / 1000 + 10, frame[2] / 10, frame[3] / 10,
                                                        frame[4] / 1000 + 30)
                        elif (len(frame) == 1):
                            send_error_data_to_android(WEBSERVER_ANDROID_DATA_FRAME_ERROR_URL, frame[0])
                    else:
                        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "NOT Correct frame!")
                    if (isSerialReceiverLoopRun == False):
                        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "serial closing")
                        serial_conn.close()
                        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "serial status: " + str(serial_conn.is_open))
                        isSerialConnClosed = True
                else:
                    None
                    print("Warning: Serial is None", file=sys.stderr)
                    serial_conn = initSerial(SERIAL_PORT, SERIAL_BAUDRATE, SERIAL_PARITY, SERIAL_STOPBITS,
                                             SERIAL_BYTESIZE)
                    make_beep()
                    time.sleep(.5)
            except SerialException as e:
                print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "isRun loop exception")
                make_beep()
                serial_conn.close()
                serial_conn = initSerial(SERIAL_PORT, SERIAL_BAUDRATE, SERIAL_PARITY, SERIAL_STOPBITS, SERIAL_BYTESIZE)
                print(str(e), file=sys.stderr)
                time.sleep(.5)


def serialTransmitter(frame):
    try:
        serial_conn.write(frame)
    except SerialException as e:
        print(str(e))


def get_available_serial_ports():
    ports = serial.tools.list_ports.comports()
    all_ports = list()

    for port, desc, hwid in sorted(ports):
        all_ports.append(dict({"port": port, "desc": desc, "hwid": hwid}))
    # print(all_ports)
    return all_ports


def get_current_serial_settings():
    serial_settings = dict()
    serial_settings = {
        "port": SERIAL_PORT,
        "baudrate": SERIAL_BAUDRATE,
        "parity": SERIAL_PARITY,
        "stopbits": SERIAL_STOPBITS,
        "bytesize": SERIAL_BYTESIZE
    }

    return serial_settings


def send_status_data_to_android(URL, hi_press, lo_press, breaths, breaths_proportion, air_volume):
    PARAMS = {'hi_press': round(hi_press, 2),
              'lo_press': round(lo_press, 2),
              'breaths': round(breaths, 2),
              'breaths_proportion': round(breaths_proportion, 2),
              'air_volume': round(air_volume, 2)
              }
    try:
        r = requests.get(url=URL, params=PARAMS)
        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, r.text)
    except requests.exceptions.RequestException as e:
        print(e.strerror)
        print("Android device is not responding!", file=sys.stderr)


def send_error_data_to_android(URL, err_code):
    PARAMS = {'err_code': round(err_code, 2)}

    try:
        r = requests.get(url=URL, params=PARAMS)
        print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, r.text)
    except requests.exceptions.RequestException as e:
        print(e.strerror)
        print("Android device is not responding!", file=sys.stderr)


def insert_frame_into_db(input):
    result = None
    cur = None
    frame = input
    cur, conn = db_connection()
    inserted = cur.execute(
        "INSERT INTO ventilaid.raw_data "
        "(hi_press, lo_press, breaths, breath_pro, air_vol) "
        "VALUES(%s, %s, %s, %s, %s);",
        (frame[0],
         frame[1],
         frame[2],
         frame[3],
         frame[4]
         ))

    conn.commit()
    gc.collect()


def print_debug(is_visable, tag, message):
    if is_visable:
        print("DEBUG TAG: " + tag + " | " + message)


def make_beep():
    mixer.init()
    mixer.music.load('resources/beep_short.mp3')
    mixer.music.play()


# Main thread
init_app_credentials()
init_webserver_credentials()
init_serial_credentials()
serial_conn = initSerial(SERIAL_PORT, SERIAL_BAUDRATE, SERIAL_PARITY, SERIAL_STOPBITS, SERIAL_BYTESIZE)
print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, str(get_available_serial_ports()))

threadLock = threading.Lock()
threads = []

# Create new threads
thread1 = myThread(threadLock, 1, "rx")
thread2 = myThread(threadLock, 2, "tx")
thread2 = myThread(threadLock, 3, "web")

# Start new Threads
thread1.start()
thread2.start()

# Add threads to thread list
threads.append(thread1)
threads.append(thread2)

# Wait for all threads to complete
for t in threads:
    t.join()
