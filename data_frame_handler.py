STATUS_FRAME_SIZE = 20
ERROR_FRAME_SIZE = 5
START_FRAME_BYTE = b'\xF0'
END_FRAME_BYTE = b'\xaa'
START_FRAME_BYTE = b'\xF0'
END_FRAME_BYTE = b'\xaa'
MES_NO_1 = b'\x01'
MES_NO_2 = b'\x02'

def catch_frame(in_bytes, start_frame_byte, end_frame_byte):
    if (len(in_bytes) == STATUS_FRAME_SIZE
            and in_bytes[0] == int.from_bytes(start_frame_byte, byteorder='big')
            and in_bytes[1] == int.from_bytes(MES_NO_1, byteorder='big')
            and in_bytes[STATUS_FRAME_SIZE - 1] == int.from_bytes(end_frame_byte, byteorder='big')
    ):
        # binary shift "<<" and "|" is used for combine bigger and little bits of 16bit variable
        hi_pres = in_bytes[2] << 8
        hi_pres = hi_pres | in_bytes[3]

        lo_pres = in_bytes[4] << 8
        lo_pres = lo_pres | in_bytes[5]

        breaths = in_bytes[6]
        breath_pro = in_bytes[7]

        air_vol = in_bytes[8] << 8
        air_vol = air_vol | in_bytes[9]

        result_frame = [hi_pres, lo_pres, breaths, breath_pro, air_vol]

        return result_frame
    elif (len(in_bytes) == ERROR_FRAME_SIZE
          and in_bytes[0] == int.from_bytes(start_frame_byte, byteorder='big')
          and in_bytes[1] == int.from_bytes(MES_NO_2, byteorder='big')
          and in_bytes[ERROR_FRAME_SIZE - 1] == int.from_bytes(end_frame_byte, byteorder='big')
    ):
        err_code = in_bytes[2]

        result_frame = [err_code]
        #print_debug(IS_DEBUG_PRINT_VISSABLE, DBG_TAG, "result frame: " + result_frame)

        return result_frame
    else:
        return None


def makeFrameByte(hi_press, lo_press,
                  breaths, breath_pro, air_vol, ref_rate,
                  f0, f1, f2, f3, f4, f5, f6):
    hi_press_0 = (hi_press >> 8) & 0xff
    hi_press_1 = hi_press & 0xff
    lo_press_0 = (lo_press >> 8) & 0xff
    lo_press_1 = lo_press & 0xff
    air_vol_0 = (air_vol >> 8) & 0xff
    air_vol_1 = air_vol & 0xff
    ref_rate_0 = (ref_rate >> 8) & 0xff
    ref_rate_1 = ref_rate & 0xff

    frame = START_FRAME_BYTE + MES_NO_1 + int(hi_press_0).to_bytes(1, "big") + int(hi_press_1).to_bytes(1, "big") \
            + int(lo_press_0).to_bytes(1, "big") + int(lo_press_1).to_bytes(1, "big") + int(breaths).to_bytes(1, "big") \
            + int(breath_pro).to_bytes(1, "big") + int(air_vol_0).to_bytes(1, "big") + int(air_vol_1).to_bytes(1, "big") \
            + int(ref_rate_0).to_bytes(1, "big") + int(ref_rate_1).to_bytes(1, "big") + int(f0).to_bytes(1, "big") \
            + int(f1).to_bytes(1, "big") + int(f2).to_bytes(1, "big") + int(f3).to_bytes(1, "big") + int(f4).to_bytes(1,
                                                                                                                      "big") \
            + int(f5).to_bytes(1, "big") + int(f6).to_bytes(1, "big") + END_FRAME_BYTE

    return frame

